# NuNet Community Feedback Process

This process aims to encourage and enable the NuNet community to actively engage in discussions, report bugs, ask questions, and suggest new features. GitLab serves as the platform to facilitate this, allowing the community to stay closely connected to the open-source code and documentation driving the NuNet platform.

In the following sections, we outline issue templates designed to guide the community in creating issues on GitLab. Moreover, we emphasize the importance of timely responses by the NuNet team. To support this, new boards, tagging strategies, and automations have been introduced.


## Templates for issues creation
To streamline the issue creation process, templates are provided with fields that guide the user. Below are the available templates and links to access them:

- [Default](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Default.md) - Provides an overview of the different templates available.
- [Bug](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Bug.md) - For reporting reproducible issues.
- [Question](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Question.md) - For asking questions or reporting non-reproducible issues.
- [Feature](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Feature.md) - For suggesting new features for the NuNet platform.
- [Discussion](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Discussion.md) - For initiating conversations or sharing thoughts on specific topics.
- [Issue](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/issue.md) - Reserved for internal use by the NuNet team for issue creation aligned with the software process.


## Issues and templates repository

NuNet hosts multiple projects on GitLab, so to maintain a consistent and standardized approach, all templates are stored in the [team-process-and-guidelines](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates) repository. To ensure consistency across projects, a script in the pipeline automatically updates relevant repositories whenever a template is modified. This automation ensures uniformity across all projects.


## Boards and tags

The following boards can be used to manage community feedback:

- [Feedback Board](https://gitlab.com/groups/nunet/-/boards/5784295): Displays all open issues submitted by the community or the NuNet team, organized with one column for each tag: ~"type::bug" ~"type::question" ~"type::feature" ~"type::discussion".
- [Bug Tracking Board](https://gitlab.com/groups/nunet/-/boards/7812932): Lists all open bugs, whether reported by community members or the NuNet team, with separate columns for each status: Open, ~"kb::requested" ~"kb::backlog" ~"kb::on hold" ~"kb::doing" ~"kb::review" ~"kb::done".

Tags used in the Bug tracking workflow:

- ~"kb::requested" – The bug is under review and analysis.
- ~"kb::backlog" – The bug has been verified.
- ~"kb::doing" – Developers are actively working on the bug.
- ~"kb::review" – The fix is currently being reviewed.
- ~"kb::done" – The bug has been resolved.

Similarly these are the tags used for managing questions, feature requests, and discussions:

- ~"kb::requested" – Someone is assigned to respond the question, analyze the feature request, or engage in the discussion.
- ~"kb::done":
    - The question has been answered.
    - A proposed feature or a discussion has led to the creation of new issues for implementation. These new issues follow our current development process and are linked to the original issue.
    - Optionally, feature requests or discussion issues can be tagged with ~"kb::backlog" for implementation.


## Automations

The following automations support the community feedback process:

- **GitLab-Slack Integration:** Connects GitLab and Slack to send messages to the support Slack channel whenever an issue with the following tags is opened or updated: ~"type::bug", ~"type::question", ~"type::feature", or ~"type::discussion".
- **Time Tracking:** Enhances the Slack integration to notify the support channel of the expiration time for open issues, ensuring timely responses.
- **Stalled Issues:** If no response is received from a community member for an extended period, the support channel is notified, allowing for a review and a decision on whether to close the issue manually.
- **Email-GitLab Integration:** Allows users to reply directly via email using GitLab’s email integration, accommodating personal preferences.
- **Template Management Automation:** A pipeline script automatically updates templates across other repositories when changes are made in the [team-process-and-guidelines](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates) repository.

## Time guidelines for community support 

NuNet aims to establish the following timelines as guidelines, particularly concerning timely responses:

### Bugs and Questions:

- Up to **12 hours**, the first response team must acknowledge the open issue and request more information if needed. If all necessary information is provided, change the tag to ~"kb::requested" and assign someone for analysis. Currently, the first response team consists of @janaina.senna.
- Up to **24 hours** after assignment, the responsible person must review the issue and add any additional information. If more time or information is required, this should be clearly stated in the issue.
- If additional information is requested, the responsible person has up to **12 hours** to review the community's response, request further feedback if necessary, or either close the issue (if the bug isn’t confirmed or the question is answered) or change the issue's tag to ~"kb::backlog" if it requires implementation.
- At this point, the issue becomes part of the development process.

### Feature Requests and Discussions:

- Up to **24 hours** the first response team must acknowledge the open issue, change the tag to ~"kb::requested", and assign someone for analysis. Currently, the first response team consists of @janaina.senna, with support from @vyzo and @dagims as needed. 
- Up to **72 hours** after assignment, the responsible person must review the issue and add any additional information. If more time is needed, this should be clearly stated in the issue.
- If feedback from a community member is requested, the responsible person has up to **24 hours** to review the community's response, request further feedback if necessary, or either close the issue if the discussion has concluded or if the feature will not be implemented. If the feature or the discussion will lead to implementation, the responsible person can change the tag to ~"kb::backlog" or create new issues (as per our development process) linked to the original community member's issue.
- At this point, the issue becomes part of the development process.

