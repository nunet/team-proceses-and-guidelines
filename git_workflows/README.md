This page contains the workflows used in GIT for the NuNet development team. The first section details the workflow from a developer's point of view, the second section details the branching strategy to organize the GIT repositories, and the third section details how version increments work for NuNet projects.

## Table of contents

- [Table of contents](#table-of-contents)
- [Git workflow - developer view](#git-workflow---developer-view)
- [Git workflow - branching strategy](#git-workflow---branching-strategy)
- [Messages for commits in NuNet projects](#messages-for-commits-in-nunet-projects)
- [Dos and Don'ts for Git Workflow](#dos-and-donts-for-git-workflow)

## Git workflow - developer view

1. The developer should use the feature branching strategy i.e. each feature/issue is implemented on its own branch.
2. From the Issue on GitLab, the developer uses the `Create branch` button. GitLab automatically fills the branch name. The developer chooses the source branch as `main` (see picture below).

<img src="../.assets/create-branch.png" width="25%" height="20%">

3. A branch tends to be short-lived, making it easier to merge.
4. A developer should commit/push every day, even when it is not yet ready for review, and write good commit messages. The developer should also merge the `main` branch every day on the feature branch.
5. When the developer is done, the developer merges the `main` branch on the feature branch, opens a merge request (MR) and assigns a peer reviewer based on one of these aspects: familiarity with the requirements, knowledge of the module or code, a useful ability, or relevant experience.
6. A MR triggers the CI pipeline. The developer makes changes if necessary so the merge request passes through the pipeline successfully.
7. Reviewers should leave questions, comments, and suggestions. Reviewers should see if the README.md files were updated. Reviewers can comment on the whole pull request or add comments to specific lines. The developer can continue to commit and push changes in response to the reviews. The MR will update automatically, and the CI pipeline will run again.
8. In addition to peer reviews, some MRs will also undergo an architectural or conceptual review, preferably conducted by @kabir.kbr or @vyzo. This review can be requested by either the MR creator or the peer reviewer.
9. Each MR needs to approved by one person from the technical board and one person from the security team.
10. Once the MR is approved, the tech lead or the product owner merges the request to `main` branch. The options to “Delete source branch” and “Squash commits” should be marked as shown in the following picture.
11. The CI pipeline runs in the `main` branch. It is unusual but, if necessary, the developer makes changes to the `main` branch so it can pass through the pipeline successfully.

<img src="../.assets/merge-request.png" width="50%" height="50%">

**Observation:** If for some reason the developer creates a branch that will not be merged, the developer needs to remove it after its use.

## Git workflow - <span dir="">branching strategy</span>

1. Feature branches are created from Issues using the GitLab interface. The developer uses the `Create branch` button. GitLab automatically fills the branch name. The developer chooses the source branch as `main` (see picture at the above section Git workflow - developer view).
2. When an issue is complete it is merged into the `main` branch through an approved merge request (MR).
3. The `release` branch tracks the current (latest) release. Code is merged/cherry picked from `main` and tags are created for every release.
4. The Release version is reached in a series of steps. <br>
  a. When code is merged/cherry picked from `main` to the release branch, it is considered as a release candidate. A tag `vX.X-rc1` is created for it. <br>
  b. A feature freeze is declared on the `main` branch. <br>
  c. Testing starts on the release candidate present on the `release` branch. <br>
  d. Bug fixes and development continue on the `main` branch. <br>
  e. This continues until we are ready for the next release candidate (`vX.X-rc2`). At this stage, `main` is merged/cherry picked into the `release` branch. <br>
  f. The above steps are repeated until we are confident there all features are incorporated and there are no known bugs left that can block the release. At this point, final release is created with the tag `vX.X`. <br>
5. Post release, the feature freeze on the `main` branch lifted. The development process continues as normal.
6. For long term development features, it may be required to create a `next` branch as an alternate to `main` while the feature freeze is in place due to the release. In this case `next` is merged to `main` and it is deleted prior to resumption of development activities.
7. For bugs discovered in development, patch releases can be created with a tag, for example `vX.X.1`. Feature branches to bugs and critical issues can be directly created from `release` branch by the developer.
8. For release with long term support, it may be needed to keep the patch release in a different branch, ex `release/vX.X.1` instead of merging to the `release` branch.
9. A label version is composed by major.minor.hotfix:
   - major version with incompatible API changes;
   - minor version with functionality in a backwards compatible manner;
   - hotfix version with backwards compatible bug fixes.


## Messages for commits in NuNet projects

Here are some common types used in messages accordingly to [Conventional Commits specification](https://www.conventionalcommits.org/en/v1.0.0/):

* feat: A new feature for the user or a significant enhancement
* fix: A bug fix
* refactor: Code changes that neither fix a bug nor add a feature
* perf: Performance improvements
* test: Adding or modifying tests
* style: Code style changes (e.g., formatting)
* docs: Documentation changes
* revert: Reverting a previous commit
* build: Changes that affect the build system or external dependencies (e.g. npm, docker, nexus)
* chore: Routine tasks, maintenance, or general refactoring
* ci: Changes to the project's Continuous Integration (CI) configuration
* release: Version number changes, typically associated with creating a new release

If the changes in the MR require updates or may break existing functionality, instead of using a type from the above list, it is necessary to use: `BREAKING CHANGE: <MR title>`

Messages added to the MR are included to the **CHANGELOG** file and appended to the release before publishing on GitLab.

The table below illustrates the mapping of MR types to version upgrade and release types.

<table>
<tr>
<th>Commit Message</th>
<th>Version Upgrade</th>
<th>Release Type</th>
</tr>
<tr>
<td>
fix<br>refactor <br>perf <br>test <br>style <br>docs 
</td>
<td>Patch</td>
<td>Fix Release</td>
</tr>
<tr>
<td>feat</td>
<td>Minor</td>
<td>Feature Release</td>
</tr>
<tr>
<td>

BREAKING CHANGE

Example:

perf(pencil): remove graphiteWidth option

BREAKING CHANGE: The graphiteWidth option has been removed. The default graphite width of 10mm is always used for performance reasons.
</td>
<td>Major</td>
<td>

Breaking Release

NOTE: The BREAKING CHANGE: token must be included at the end of the commit message.
</td>
</tr>
<tr>
<td>build<br>chore <br>ci</td>
<td>None</td>
<td>No new release created</td>
</tr>
</table>


## Dos and Don'ts for Git Workflow

1. Isolate Features in Dedicated Branches

- Reason: Ensures that changes remain modular and easy to review.
- How: Create a new branch for each feature or bug fix, such as `feature/xyz` or `bugfix/abc`.
- Tip: Use descriptive branch names reflecting the branch’s purpose for better navigation.

2. Commit Early and Often

- Reason: Committing small, incremental changes improves version control and debugging.
- How: Break tasks into smaller chunks and commit frequently with meaningful messages.
- Tip: Avoid vague messages like 'fix bug'; instead, use 'Fix dashboard layout rendering issue.'

3. Rebase or Merge Regularly

- Reason: Keeps feature branches up-to-date with the main branch, avoiding merge conflicts.
- How:
  - For short-lived branches, use `git rebase origin/main`.
  - For long-lived branches, use `git merge origin/main`.
- Tip: Regularly review changes after rebase or merge and run tests to ensure stability.

4. Use Merge Requests (MRs) for Code Review

- Reason: Ensures code quality and documents the reasoning behind changes.
- How: Open an MR when ready for review, with detailed descriptions and related tickets.
- Tip: Include tests and comprehensive explanations in the MR for clarity.

5. Test Features in Isolation

- Reason: Prevents regressions or feature interactions, improving stability.
- How: Use continuous integration (CI) pipelines to automatically test each feature branch.
- Tip: Use GitLab CI/CD pipelines to enforce passing tests before merging.

6. Avoid Merging Feature Branches Into Each Other Prematurely

- Reason: Creates dependencies and complicates tracking, leading to conflicts or bugs.
- How: Wait until each branch is complete and reviewed before merging into the main branch.
- Learning: Keep features independent to simplify testing and review.

7. Don’t Bypass Code Reviews or Testing

- Reason: Skipping reviews or tests introduces risks like bugs or performance issues.
- How: Ensure all changes go through MRs with proper reviews, and CI tests pass.
- Learning: Following thorough reviews reduces future rework.

8. Don’t Push a Feature Branch Into Another as a Shortcut

- Reason: Merging one branch into another for speed complicates history and debugging.
- How: Merge each branch individually into the main branch or use an 'integration' branch for combined testing.
- Learning: Stick to independent merges to maintain a clean commit history.

9. Don’t Force Push Without Caution

- Reason: Force pushing can overwrite commits, leading to data loss or conflicts.
- How: Use `git push --force` only when necessary, or use `git push --force-with-lease` to avoid overwriting others' work.
- Tip: Avoid force pushing to the main branch unless absolutely necessary.

10. Don’t Delay Merging for Too Long

- Reason: Delaying merges increases the risk of conflicts and makes tracking harder.
- How: Rebase or merge regularly and aim to merge branches as soon as they are reviewed.
- Tip: Adopt 'merge early, merge often' to minimize conflicts and keep the codebase up to date.
