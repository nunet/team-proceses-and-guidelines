## **Table of contents**

- [Contributing guidelines](#contributing-guidelines)
  - [Before you get started](#before-you-get-started)
  - [How to report a bug?](#how-to-report-a-bug)
  - [How to ask a question or report a non reproducible issue?](#how-to-ask-a-question-or-report-a-non-reproducible-issue)
  - [How to request a new feature?](#how-to-request-a-new-feature)
  - [How to make a code contribution?](#how-to-make-a-code-contribution)
    - [Finding an issue](#finding-an-issue)
    - [Understand the development process](#understand-the-development-process)
    - [Implementation](#implementation)
  - [How to initite a discussion?](#how-to-initite-a-discussion)


# Contributing guidelines

This page contains guidelines for the community to contribute in NuNet's development. We appreciate any kind of contribution, including:
- Bug reporting and fixes
- Documentation improvements
- Code contributions & improvements
- New features
- Testing etc

## Before you get started
Please take a look at **Device Management Service (DMS) [README](https://gitlab.com/nunet/device-management-service/-/blob/develop/README.md)** to get an overall understanding of the project architecture and the underlying concepts. This will also tell you basics of how to get started with using the DMS functionality.

It is recommended that you familiarize yourself with the codebase. Read the associated documentation specified in the `README` files present in each directory. To understand how the `README` files are structured refer to the **[Specification and Documentation Framework](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/specification_and_documentation/README.md#specification-framework)**

## How to report a bug?

- Bug reports should be submitted to the [issue tracker](https://gitlab.com/groups/nunet/-/issues) on the appropriate repository. For example, if the bug about Device Management Service (DMS), use the **[DMS issue tracker](https://gitlab.com/nunet/device-management-service/-/issues)**.

**Note:** Bugs should be **reproducible!** Include detailed steps on how to reproduce the problem, along with any error messages. Screenshots and GIFs are helpful too! If you are unsure if what you are experiencing is reproducible, reach out to our community and ask help to reproduce what you need in order to confirm!

- [Always search the Bug Tracking Board FIRST](https://gitlab.com/groups/nunet/-/boards/7812932)! It’s likely someone caught this before you, or already reported something similar, and it will save time and effort. If you find an existing issue, show your support with an award emoji and/or join the discussion.

- [Use the Bug Issue Template](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Bug.md)! This template can be chosen while creating a new issue. See below for an illustrative screenshot.

<img src="../.assets/bugTemplate.png" width="50%" height="50%">

The boxes in template have some prompters to help you on what should be included. The template can be populated and edited as needed.

- Add the ~type::bug label to your issue!

## How to ask a question or report a non reproducible issue?

- Questions should be submitted to the [issue tracker](https://gitlab.com/groups/nunet/-/issues) on the appropriate repository. For example, if the question is about Device Management Service (DMS), use the **[DMS issue tracker](https://gitlab.com/nunet/device-management-service/-/issues)**.

- [Always search the Feedback Board FIRST](https://gitlab.com/groups/nunet/-/boards/5784295)! It's likely that someone already reported something similar, and it will save time and effort. If you find an existing issue, show your support with an award emoji and/or join the discussion.
  
- Use the [Questions template](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Question.md)! This template can be chosen while creating a new issue. See below for an illustrative screenshot. 

<img src="../.assets/questionTemplate.png" width="50%" height="50%">

- Add the ~type::question label to your issue!

## How to request a new feature?

Feature proposals should be submitted to the [**issue tracker**](https://gitlab.com/groups/nunet/-/issues).

1. Refer to the [Update Procedure](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/specification_and_documentation/README.md#update-procedure) for steps to be followed while suggesting a change to the existing functionality.

2. Keep it simple! Keep feature proposals as small and simple as possible, complex ones might be edited to make them small and simple.

3. [Always search the Feedback Board FIRST](https://gitlab.com/groups/nunet/-/boards/5784295)! It’s likely someone already reported something similar, and it will save time and effort. If you find an existing issue, show your support with an award emoji and/or join the discussion.

4. [Use the Feature Proposal Issue Template](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Feature.md)! This template can be chosen while creating a new issue. See below for an illustrative screenshot.

<img src="../.assets/featureTemplate.png" width="50%" height="50%">

The boxes in template have some prompters to help you on what should be included. The template can be populated and edited as needed.

5. Add the ~type::feature label to your issue!


## How to make a code contribution?

### Finding an issue

1. Search the [Issue Tracker](https://gitlab.com/groups/nunet/-/issues?label_name=kb::backlog) to look at already created issues ready for development. 

Apply the filter with ~good-first-issue label to see issues suitable for those who are new to the project.

### Understand the development process
2. Familiarise yourself with the [Specification and Documentation Framework](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/specification_and_documentation/README.md#specification-framework).

3. [Here](https://gitlab.com/nunet/team-processes-and-guidelines/-/tree/main/git_workflows) you can find the development workflow used in NuNet platform. 


### Implementation

4. Refer to [these steps](https://gitlab.com/nunet/main/-/blob/main/specification_and_documentation/README.md#implementation) for the process to be followed while implementing a new feature or functionality.

5. Create a merge request with the contributed code, filling out all requested information accordingly to the [merge request template](https://gitlab.com/nunet/device-management-service/-/blob/main/.gitlab/merge_request_templates/Default.md).

6. After submitting the merge request, verify that all CI/CD pipeline stages are running successfully. Fix the merge request if necessary.

7. All code and contributions have to include appropriate documentation updates, corresponding to the code changes, as explained in [Specification and Documentation](https://gitlab.com/nunet/main/-/blob/main/specification_and_documentation/README.md) `README` file.

## How to initite a discussion?

- To start a conversation or to share your thoughts on a specific topic, a new issue should be submitted to the [issue tracker](https://gitlab.com/groups/nunet/-/issues) on the appropriate repository. For example, if the question is about Device Management Service (DMS), use the **[DMS issue tracker](https://gitlab.com/nunet/device-management-service/-/issues)**.

- [Always search the Feedback Board FIRST](https://gitlab.com/groups/nunet/-/boards/5784295)! It's likely that someone already reported something similar, and it will save time and effort. If you find an existing issue, show your support with an award emoji and/or join the discussion.
  
- Use the [Discussion template](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Discussion.md)! This template can be chosen while creating a new issue. See below for an illustrative screenshot. 

<img src="../.assets/discussionTemplate.png" width="50%" height="50%">

- Add the ~type::discussion label to your issue!


