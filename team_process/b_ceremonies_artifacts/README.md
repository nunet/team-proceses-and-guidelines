# Ceremonies and Artifacts at NuNet

At NuNet, we’ve adopted the following ceremonies and developed automations to help us monitor essential resources and prioritize dependent tasks, so we can complete milestones as efficiently as possible:

- **Critical Chain Daily Meetings:** These meetings are held daily to monitor critical chain progress, ensuring projects/milestones stay on track. Mandatory attendance for the milestone technical owner and individuals directly or indirectly involved in critical chain issues. Optional for other developers unless their attendance is directly requested by a team member on a critical chain AND/OR in case of not providing an async update in a WIP GitLab issue. 
- **Weekly Tech All-Hands:** A weekly obligatory meeting for all engineering team members aimed at providing clarity on broader context and tech-related updates.
- **Ad-hoc Sync Meetings:** Scheduled sessions to discuss issues in detail, identify blockers, and set the course for resolution.
- **Automatic Processes:** Implementing automation to update the Kanban board and post messages to Slack channels regarding updates related to Critical Chain Project Management (CCPM) and Kanban.

## Critical Chain Daily Meeting

- **Definition:** Daily meeting to discuss the critical chain of a milestone with a locked project buffer (=defined scope of the milestone) as per CCPM methodology.
- **Purpose:** Monitor progress along the critical chain to maintain project/milestone alignment and enable developers to streamline technical collaboration and move fast with the issues on the critical chain (see the section at the end of this document about how Drum Buffer Rope (DBR) methodology works - [drum buffer roll](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/c_drum_buffer_rope/README.md)).
- **Duration:** 15-to-30 minutes
- **Attendance:** Mandatory attendance for the milestone technical owner and individuals directly or indirectly involved in critical chain issues (see below). Optional for other developers unless their attendance is directly requested by a team member on a critical chain AND/OR in case of not providing an async update in a WIP GitLab issue.
- An automatic message is posted in the `#status-update-<milestone>` Slack channel each day, listing the critical chain work packages and the developers associated with them (as shown in the picture below).
- Developers with direct involvement in the critical chain can request attendance from others indirectly involved by sending a message in the `#status-update-<milestone>` Slack channel.
- If multiple milestones have locked project buffers, separate meetings will be held for each milestone. In other words, if a developer is on a critical chain in both milestones with locked project buffers, they will be expected to attend two daily meetings.

<img src="../../.assets/CriticalChainDailyMeeting.png" width="60%" height="60%">

## Weekly Tech All-hands

- **Definition:** One meeting per week with the whole team to update about all milestones.
- **Purpose:** Provide comprehensive clarity on the broader context, ensuring that the development team understands the overarching scope of NuNet projects.
- **Duration:** 30 minutes
- **Attendance:** Obligatory attendance for all tech team members.
- **Responsibility:** The milestone owner is responsible for updates. The owner can choose to share the word with others to give a better view of the update if needed.
- Create a summary written record of those meetings, and post it in the `#status-update` Slack channel that is used to general updates about the milestones.
- Send the updated portfolio fever chart every week to the `#status-update` Slack channel.

## Ad-hoc Sync Meetings

- **Definition:** Periodical synchronization meetings according to the needs.
- **Purpose:** Provide an opportunity for everyone to update and discuss their issues. Also important to highlight blockers and set the course.
- **Suggested duration:** 30 minutes to one hour
- **Responsibility:** Responsibility to create and manage these meetings is up to the team leads. Each team lead can schedule it from every day to once a week depending on the project and team’s needs.
- The team lead can also schedule more than one meeting, splitting the team, according to the topics being discussed. 
- The team leads (Kabir, Dagim, Janaina) can also schedule meetings with people from different teams.
- In summary, the idea is to have ad hoc meetings according to the needs at the moment.
- The team leads need to make a clear and shared agenda for these sync meetings. If there is nothing on the agenda 1-day prior to the scheduled meeting, just skip it.
- In addition to these synchronization meetings, asynchronous collaboration via GitLab issues, research blogs, and documentation is essential due to our distributed context, open source projects, community participation, and multiple time zones.

## Other Meetings Involving the Development Team

- **Weekly company-wide All-hands:** A 30-minute meeting once a week to share general updates with the entire NuNet team.
- **Technical discussions:** One hour meeting dedicated to discussing specific topics that are important to the development team. Once a topic is defined, team participation will be suggested so individuals can determine whether their attendance is necessary. 
- **Retrospective:** These sessions will be scheduled on demand to discuss the development process with all the development team.
- **Kick-off meetings:** Held to start a new project following the CCPM methodology.
- **Mission control:** Conducted to address critical issues urgently.
- **Code review sessions:** These sessions will be scheduled on demand to ensure code quality and consistency while promoting team collaboration and knowledge sharing.

## Technical Management Board at NuNet

Technical management board consists of Kabir, Dagim, Janaina and Vyzo. The goal of this board is to agree on high-level architectural concepts and align communication to the team. Concrete concerns/aspects to take into account and solve by this board:

- **Kabir** is responsible for the high-level conceptual architecture of the platform and the process of translating the concepts (actor model, graph traversals, etc.) into specifications that can be implemented by the team.
- **Vyzo** is responsible for translating high-level requirements and architectural principles into implementable technical designs and prototypes to validate architectural decisions and work with Dagim and the development team to ensure prototypes are scalable and can be transitioned into full production systems.
- **Dagim** is responsible for the coordination of low-level development and code refactoring work, communicating with developers, etc. For that, high-level conceptual architecture has to be aligned with the low-level tech possibilities and constraints.
- **Janaina** is responsible for the project management process, task and issue alignments, milestone designs, etc., and thus often needs to relate high-level architectural concepts and decisions with everyday tasks and issues on a lower level.

**Meetings:**

- **Tech board alignment:** Weekly architecture alignment call 30-60 minutes for in-depth discussion of the architectural aspects to be implemented.
- **Daily alignment call:** 15 minutes for:
  - Identifying aspects that need to be aligned and communicated to the team;
  - Distributing technical leads to the respective priorities.

