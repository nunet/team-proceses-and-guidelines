
# How Drum Buffer Rope (DBR) works

<img src="../../.assets/DrumBufferRope.png" width="60%" height="60%">

- **Drum:** The drum is the constraint, which sets the pace for the entire system, much like a drumbeat. In a software development setting, this might be a particular stage in the workflow that is the slowest or has the least capacity.

- **Buffer:** This is a time buffer placed before the constraint to ensure it is always fed with work and never starved. This helps to accommodate variability and keep the constraint working continuously. In software terms, it could mean prioritizing tasks so that the development team (the constraint) always has a backlog of ready work.

- **Rope:** This is the mechanism that controls the release of new work into the system. The rope ensures that work is introduced at a rate that the constraint can handle, preventing excessive work-in-progress (WIP) that can lead to bottlenecks and delays.


## Simplified DBR (sDBR)

sDBR is just normal DBR without a constraint buffer and has only a project buffer, as in our case. The market / release plans are considered the constraint and the drum is set to meet all the due dates. When you have a market constraint, you need to explicit that constraint by making sure you are on time to all customers.


In sDBR, the drum is the due date. Therefore there is no need to sequence jobs at the constraint or the ‘would be’ capacity constrained resource (CCR) since the constraint is the market. Raw materials (tasks / work to be done) are released to the team on the task due date (scheduled in the gantt chart) minus the project buffer. **Releasing the right jobs in the right order is critical. Therefore the most important thing is to set priorities for jobs / tasks.**


## Integration with Agile and Kanban

DBR is a ‘pull’ system. When constraint finishes a task, another task is released into the system (from the backlog). Kanban is a ‘don’t push’ system - if the Kanban board is full we do not push more into the WIP / doing. In DBR, the buffer is time; In Kanban, the buffer is space.


Kanban's ‘don’t push’ system aligns well with the rope aspect of DBR, controlling WIP by only pulling new work when the current work is completed. This reduces the risk of overburdening the system and helps maintain a smooth workflow. DBR adds an extra layer by focusing on the constraint and ensuring it's always productive.


## Relevant links

- Project management page for the milestone: https://docs.nunet.io/project-management-portal/device-management-service-version-0-5-x

- **All About Lean** provides an in-depth look at the DBR method and compares it with other methodologies like Kanban, discussing its advantages and potential drawbacks: All About Lean - A Critical Look at Goldratt's Drum-Buffer-Rope Method (AllAboutLean.com).

- **Velocity Scheduling System** offers a comprehensive summary of DBR, including simplified versions and practical applications in scheduling: Velocity Scheduling System - Drum Buffer Rope Summary (Velocity Scheduling System).

- **Smartsheet** discusses how DBR can be applied within Agile and Kanban frameworks, providing insights into its implementation in software development: Smartsheet - All About Kanban Software Development (Smartsheet).

