# Team Collaboration Culture and Rules of Engagement

## Introduction

Considering our remote setup and the Open Source nature of our project, written communication is like the lifeblood or oxygen of our globally distributed team. Without it, we can’t function efficiently, let alone integrate a network of global Open Source contributors. Without strong and asynchronous communication, we will struggle with duplicate work, dependencies, conflicts, and misunderstandings.

To ensure our success, every engineering team member, whether on the critical team or not, should adhere to a set of core principles. These principles are designed to promote effective communication, streamline workflows, and maintain high-quality standards across our distributed team. Please find the aforementioned principles in the following subpages.

## Critical Chain Daily Meeting

We expect mandatory attendance for the milestone technical owner and individuals directly or indirectly involved in critical chain issues. It is optional for other developers unless their attendance is directly requested by a team member on a critical chain **AND/OR** in case of not providing an async update in a WIP GitLab issue.

**Why is this important?**  
Critical chain issues are crucial as they essentially make or break the milestone. Sharing daily progress and blockers with the team and technical leadership is vital. Therefore, if you’re working on a critical chain issue directly or indirectly, you must attend a synchronous daily meeting.

## Async Updates in GitLab

All tech team members need to update their work-in-progress GitLab issues with a comment every day. If no comments are submitted, it’s assumed that no progress has been made, and your presence in the daily meeting is required, regardless of whether the issue is on the critical chain or not.

**Why is this important?**  
Imagine NuNet with 100 software engineers and twice as many OS community developers. You find an interesting issue that someone else has been working on, but there’s no information on what’s been done or what needs to be completed. You end up reverse engineering their code. It would be much more efficient if they had left an update with a to-do list in the comments. Remember, communication fuels us forward more efficiently.

## GitLab as Our Main Communication Platform

Considering the Open Source nature of our project, NuNet needs GitLab as the main communication platform for all tech team members, both internal and external (OS developers).

**Why is it important?**  
Otherwise, communication, the oxygen fueling NuNet Platform success, gets trapped and siloed. Avoid technical discussions in Slack or DMs to prevent siloed communication. This ensures transparency and accessibility for everyone, especially for the external Open Source Contributors.

## Self-Assigning to Issues

Everyone on the team should always have an issue assigned to them. It’s each team member’s responsibility to assign themselves an issue. If you don’t have an issue or have finished one, pick a new one from the backlog and inform Janaina via comment. If more technical context is required, reach out to Kabir, Dagim, or Janaina via GitLab comments for increased visibility.

**Why is this important?**  
Picture the development process with numerous OS contributors. As a milestone or WP owner, you’re leading Mainnet integration with Cardano. Imagine 20 developers finishing their issues simultaneously and asking you what to work on next. Instead of reviewing the contributions, you spend your morning assigning issues top-down. You don’t feel like going on holidays because you’re constantly worried the backlog won’t move forward with the same velocity. Not the nicest feeling, is it?

Now, imagine the same scenario but with these 20 developers self-assigning issues from the backlog based on set priorities, their experience levels, and interests, and only reaching out for technical questions if needed. Now ask yourself: which option sounds more efficient and less frustrating? Which of these 20 developers would you prefer to work with? In which group do you see yourself thriving as a developer?

## Live and Breathe Quality Code

We aim for approximately 90% test coverage to ensure everything is tested and to facilitate quicker merges. Every MR must include unit tests for new functionalities or changes to existing functionalities. Otherwise, the MR will be rejected by default and pushed back to the developer to add unit tests.

## Summary

There are five core principles we expect all engineering team members to follow for efficient coordination in our remote setup:

1. Attend daily meetings if you’re on the critical chain.
2. Submit daily technical progress updates on issues.
3. Use GitLab for all communication, updates, comments, and technical discussions.
4. Be proactive and self-assign issues based on the broader priorities in the milestone.
5. Contribute to our code quality by including unit tests in every MR.

And remember: **I will communicate as much as possible because it’s the oxygen of a globally distributed company.**

Last but not least, if you have any better ideas on how to ensure coordination and efficiency in our distributed setup and want to add or amend the dev process at NuNet, please be vocal and contribute! While we believe the five core principles above help us to be an efficient team, we’re open to continuous improvement and learning how to build things better. Submit your suggestions via merge requests to this file, indicating @janainasenna as reviewer.


