# Development Process at NuNet

This documentation provides an overview of the processes, procedures, and frameworks used at NuNet to enhance the development workflow.

Useful documentation related to the development process:
- [GIT workflows and branching strategy](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/git_workflows/README.md)
- [MR review process](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/e_mr_review/README.md)
- [Best practices](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/best_practices/README.md)
- [Documentation](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/specification_and_documentation/README.md)


## Kanban

NuNet development team process is based on Kanban, a visual project management methodology aimed at optimizing workflow efficiency and flexibility. Its primary objectives include visualizing work to enhance transparency, limiting work in progress (WIP) to prevent bottlenecks, and focusing on continuous delivery and improvement. The issues are visible on [this board](https://gitlab.com/groups/nunet/-/boards/4356023) and we move them through stages: `backlog`, `doing`, `review` and `done`. If an issue has a blocker, we put it on stage `on hold` to make it visible allowing the team to clearly understand why progress on certain issues has stalled and what is required to move them forward.

## Automations

An automatic message (as shown in the picture below) is posted in the `#status-update` Slack channel each day, listing:
- team members without assigned issues;
- issues with one day left;
- issues with no assignee;
- issues with no weight (the weight represents the estimated days to finish the issue).

<img src="../../.assets/DailyReport.png" width="60%" height="60%">

An automatic script scheduled to run once daily at 23h UTC from Monday to Friday decreases the expectation days (weight) by one for issues with the `kb::doing` label except if:
- the issue has only one day left;
- the team member is marked as AFK (away from keyboard) in internal NuNet Calendar.

## Procedures

Note: Refer to the [Project Management Documentation](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/a_project_management/README.md) to better understand some terms used in this section.

- Developers review the message posted in the `#status-update` Slack channel before the critical chain daily meeting. If their name is mentioned, it indicates that they need to take action, such as assigning an issue to themselves, setting the weight for an issue, moving an issue to review, and so forth.

- If developers need to change the weight of their issues, they should do so directly in GitLab, also adding a comment explaining the motivation. If the issue belongs to a work package in the critical chain, the work package owner should be copied on the comment, as this will impact the project buffer. The same process applies when creating new issues.

- Once a week, the work package owner should review the work package weight that represents the Estimated Time To Completion (ETTC), which is automatically updated by the pipeline at 23h UTC based on all open issues within the work package.

- During the review, the work package owner should:
   - Link any new issues to the work package if they are not already linked.
   - Analyze whether new issues need to be created and create them as necessary.
   - Manually run the pipeline if any updates are made that may impact the ETTC.
   - Evaluate if the ETTC represents the time required to complete the work package and inform the milestone owner.


