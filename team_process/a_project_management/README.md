# Project Management at NuNet

At NuNet, we’ve adopted Critical Chain Project Management (CCPM), a [methodology](https://asana.com/resources/project-management-methodologies) that emphasizes resource availability and [task dependencies](https://asana.com/resources/project-dependencies) to ensure timely and efficient project completion. The next sections give an overview of the key concepts of CCPM applied in NuNet.

## Critical Chain

The critical chain is the longest sequence of dependent tasks in a project, considering both task dependencies and resource constraints. Unlike the critical path, which only considers task dependencies, the critical chain also accounts for the availability of resources required to perform tasks, often making it longer than the critical path. In NuNet, a [Gantt Chart](https://docs.nunet.io/docs/v/project-management-portal/device-management-service-version-0-5-x/current-gantt-chart) is used to monitor each project. Tasks highlighted in red represent the critical chain and determine the overall project duration. Non-critical tasks are represented in blue.

## Project Buffer and Feeding Buffers

In traditional methods, task durations are usually estimated conservatively to include safety margins. Critical Chain Project Management (CCPM), however, uses optimistic estimates and places safety margins into buffers instead. The project buffer is a time buffer placed at the end of the critical chain to protect the project completion date from delays. A feeding buffer is placed where non-critical tasks feed into the critical chain, protecting the critical chain from delays in these feeding paths. The health of the project is monitored by observing buffer consumption. If buffers are being consumed faster than planned, it signals potential delays, allowing project managers to take corrective actions before the project is endangered.

## Resource Management

CCPM focuses on ensuring that resources are not over-committed and are available when required for critical tasks. The approach encourages multi-tasking reduction, ensuring that resources can concentrate on one task at a time for better efficiency.

## Portfolio Management

In CCPM, a [Fever Chart](https://docs.nunet.io/docs/v/project-management-portal/device-management-service-version-0-5-x/current-feverchart) is a visual tool used to monitor the overall health of multiple projects within a portfolio. It tracks the progress of each project by comparing buffer consumption (the time or resources used up compared to what was allocated) against project completion percentage. The chart typically uses a color-coded system of green, yellow, and red zones to indicate whether a project is on track, at risk, or in critical condition respectively. By visualizing the status of all projects in a single chart, the portfolio fever chart helps managers quickly identify which projects need attention and prioritize resources accordingly, ensuring that the entire portfolio remains on course for successful completion.

