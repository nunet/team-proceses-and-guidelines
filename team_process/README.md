# Description

This document outlines the project management and technical development processes at NuNet, engineering meetings and ceremonies, and the rules of engagement for the Engineering Team Members. Contributors have comment access, and team members are invited to submit suggestions via merge requests to this file, with @janainasenna indicated as the reviewer.

- [Project Management](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/a_project_management/README.md)
- [Ceremonies and Artifacts](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/b_ceremonies_artifacts/README.md)
- [Drum Buffer Rope (DBR)](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/c_drum_buffer_rope/README.md)
- [Development Process](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/d_development_process/README.md)
- [Team Collaboration Culture and Rules of Engagement](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/e_culture_rules/README.md)
- [Merge Request Review Process](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/team_process/f_mr_review/README.md)

